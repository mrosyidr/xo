function xo(str) {
  let numOfx = 0;
  let numOfo = 0;
  for(let item of str) {
    if(item === 'x')
      numOfx++;
    else if(item === 'o')
      numOfo++;
  }
  return numOfx == numOfo;
}

// TEST CASES
console.log(xo('xoxoxo')); // true
console.log(xo('oxooxo')); // false
console.log(xo('oxo')); // false
console.log(xo('xxxooo')); // true
console.log(xo('xoxooxxo')); // true